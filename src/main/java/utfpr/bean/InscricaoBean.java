package utfpr.bean;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import utfpr.entity.Inscricao;
import utfpr.faces.support.PageBean;
import utfpr.jpa.InscricaoJpa;

@ManagedBean
@RequestScoped
public class InscricaoBean extends PageBean {

	public List<Inscricao> getInscricoes() {
		InscricaoJpa ctrl = new InscricaoJpa();
		List<Inscricao> inscricoes = ctrl.findAll();
		return inscricoes;
	}

}
