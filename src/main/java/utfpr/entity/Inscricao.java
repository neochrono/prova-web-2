package utfpr.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the inscricao database table.
 * 
 */
@Entity
@NamedQuery(name="Inscricao.findAll", query="SELECT i FROM Inscricao i")
public class Inscricao implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer numero;

	@Column(name="atuacao_empresa")
	private String atuacaoEmpresa;

	private String bairro;

	private Integer categoria;

	private Integer cep;

	private String cidade;

	private String complemento;

	@Column(name="complemento_categoria")
	private String complementoCategoria;

	private BigDecimal cpf;

	@Column(name="data_hora")
	private Timestamp dataHora;

	private String email;

	private String endereco;

	private String estado;

	private String fone;

	private String nome;

	private Boolean participacao;

	private Integer situacao;

	//bi-directional many-to-one association to InscricaoMinicurso
	@OneToMany(mappedBy="inscricao")
	private List<InscricaoMinicurso> inscricaoMinicursos;

	public Inscricao() {
	}

	public Integer getNumero() {
		return this.numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public String getAtuacaoEmpresa() {
		return this.atuacaoEmpresa;
	}

	public void setAtuacaoEmpresa(String atuacaoEmpresa) {
		this.atuacaoEmpresa = atuacaoEmpresa;
	}

	public String getBairro() {
		return this.bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public Integer getCategoria() {
		return this.categoria;
	}

	public void setCategoria(Integer categoria) {
		this.categoria = categoria;
	}

	public Integer getCep() {
		return this.cep;
	}

	public void setCep(Integer cep) {
		this.cep = cep;
	}

	public String getCidade() {
		return this.cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getComplemento() {
		return this.complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getComplementoCategoria() {
		return this.complementoCategoria;
	}

	public void setComplementoCategoria(String complementoCategoria) {
		this.complementoCategoria = complementoCategoria;
	}

	public BigDecimal getCpf() {
		return this.cpf;
	}

	public void setCpf(BigDecimal cpf) {
		this.cpf = cpf;
	}

	public Timestamp getDataHora() {
		return this.dataHora;
	}

	public void setDataHora(Timestamp dataHora) {
		this.dataHora = dataHora;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEndereco() {
		return this.endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getEstado() {
		return this.estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getFone() {
		return this.fone;
	}

	public void setFone(String fone) {
		this.fone = fone;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Boolean getParticipacao() {
		return this.participacao;
	}

	public void setParticipacao(Boolean participacao) {
		this.participacao = participacao;
	}

	public Integer getSituacao() {
		return this.situacao;
	}

	public void setSituacao(Integer situacao) {
		this.situacao = situacao;
	}

	public List<InscricaoMinicurso> getInscricaoMinicursos() {
		return this.inscricaoMinicursos;
	}

	public void setInscricaoMinicursos(List<InscricaoMinicurso> inscricaoMinicursos) {
		this.inscricaoMinicursos = inscricaoMinicursos;
	}

	public InscricaoMinicurso addInscricaoMinicurso(InscricaoMinicurso inscricaoMinicurso) {
		getInscricaoMinicursos().add(inscricaoMinicurso);
		inscricaoMinicurso.setInscricao(this);

		return inscricaoMinicurso;
	}

	public InscricaoMinicurso removeInscricaoMinicurso(InscricaoMinicurso inscricaoMinicurso) {
		getInscricaoMinicursos().remove(inscricaoMinicurso);
		inscricaoMinicurso.setInscricao(null);

		return inscricaoMinicurso;
	}

}