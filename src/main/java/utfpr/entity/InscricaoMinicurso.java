package utfpr.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the inscricao_minicurso database table.
 * 
 */
@Entity
@Table(name="inscricao_minicurso")
@NamedQuery(name="InscricaoMinicurso.findAll", query="SELECT i FROM InscricaoMinicurso i")
public class InscricaoMinicurso implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private InscricaoMinicursoPK id;

	@Column(name="data_hora")
	private Timestamp dataHora;

	private Integer situacao;

	//bi-directional many-to-one association to Inscricao
	@ManyToOne
	@JoinColumn(name="numero_inscricao")
	private Inscricao inscricao;

	//bi-directional many-to-one association to Minicurso
	@ManyToOne
	@JoinColumn(name="minicurso")
	private Minicurso minicursoBean;

	public InscricaoMinicurso() {
	}

	public InscricaoMinicursoPK getId() {
		return this.id;
	}

	public void setId(InscricaoMinicursoPK id) {
		this.id = id;
	}

	public Timestamp getDataHora() {
		return this.dataHora;
	}

	public void setDataHora(Timestamp dataHora) {
		this.dataHora = dataHora;
	}

	public Integer getSituacao() {
		return this.situacao;
	}

	public void setSituacao(Integer situacao) {
		this.situacao = situacao;
	}

	public Inscricao getInscricao() {
		return this.inscricao;
	}

	public void setInscricao(Inscricao inscricao) {
		this.inscricao = inscricao;
	}

	public Minicurso getMinicursoBean() {
		return this.minicursoBean;
	}

	public void setMinicursoBean(Minicurso minicursoBean) {
		this.minicursoBean = minicursoBean;
	}

}