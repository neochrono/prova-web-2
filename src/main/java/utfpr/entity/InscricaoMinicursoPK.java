package utfpr.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the inscricao_minicurso database table.
 * 
 */
@Embeddable
public class InscricaoMinicursoPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="numero_inscricao", insertable=false, updatable=false)
	private Integer numeroInscricao;

	@Column(insertable=false, updatable=false)
	private Integer minicurso;

	public InscricaoMinicursoPK() {
	}
	public Integer getNumeroInscricao() {
		return this.numeroInscricao;
	}
	public void setNumeroInscricao(Integer numeroInscricao) {
		this.numeroInscricao = numeroInscricao;
	}
	public Integer getMinicurso() {
		return this.minicurso;
	}
	public void setMinicurso(Integer minicurso) {
		this.minicurso = minicurso;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof InscricaoMinicursoPK)) {
			return false;
		}
		InscricaoMinicursoPK castOther = (InscricaoMinicursoPK)other;
		return 
			this.numeroInscricao.equals(castOther.numeroInscricao)
			&& this.minicurso.equals(castOther.minicurso);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.numeroInscricao.hashCode();
		hash = hash * prime + this.minicurso.hashCode();
		
		return hash;
	}
}