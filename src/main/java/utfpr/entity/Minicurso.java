package utfpr.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the minicurso database table.
 * 
 */
@Entity
@NamedQuery(name="Minicurso.findAll", query="SELECT m FROM Minicurso m")
public class Minicurso implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer codigo;

	private String descricao;

	private String ministrante;

	private Integer vagas;

	//bi-directional many-to-one association to InscricaoMinicurso
	@OneToMany(mappedBy="minicursoBean")
	private List<InscricaoMinicurso> inscricaoMinicursos;

	public Minicurso() {
	}

	public Integer getCodigo() {
		return this.codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return this.descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getMinistrante() {
		return this.ministrante;
	}

	public void setMinistrante(String ministrante) {
		this.ministrante = ministrante;
	}

	public Integer getVagas() {
		return this.vagas;
	}

	public void setVagas(Integer vagas) {
		this.vagas = vagas;
	}

	public List<InscricaoMinicurso> getInscricaoMinicursos() {
		return this.inscricaoMinicursos;
	}

	public void setInscricaoMinicursos(List<InscricaoMinicurso> inscricaoMinicursos) {
		this.inscricaoMinicursos = inscricaoMinicursos;
	}

	public InscricaoMinicurso addInscricaoMinicurso(InscricaoMinicurso inscricaoMinicurso) {
		getInscricaoMinicursos().add(inscricaoMinicurso);
		inscricaoMinicurso.setMinicursoBean(this);

		return inscricaoMinicurso;
	}

	public InscricaoMinicurso removeInscricaoMinicurso(InscricaoMinicurso inscricaoMinicurso) {
		getInscricaoMinicursos().remove(inscricaoMinicurso);
		inscricaoMinicurso.setMinicursoBean(null);

		return inscricaoMinicurso;
	}

}